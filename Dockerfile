FROM golang:alpine

RUN apk update && apk add --no-cache git

RUN go get gitlab.com/Niesch/go-voting

WORKDIR /go/src/gitlab.com/Niesch/go-voting

CMD ["/go/bin/go-voting"]
