package main

import (
	"net/http"
	"log"
)

func main() {
	vl := &VotingList{}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		indexHandler(w, r, vl)
	})
	http.HandleFunc("/new", func(w http.ResponseWriter, r *http.Request) {
		newVoteHandler(w, r, vl)
	})
	http.HandleFunc("/remove", func(w http.ResponseWriter, r *http.Request) {
		removeHandler(w, r, vl)
	})
	http.HandleFunc("/vote", func(w http.ResponseWriter, r *http.Request) {
		castVoteHandler(w, r, vl)
	})
	
	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	log.Fatal(http.ListenAndServe(":8001", nil))
	
}
